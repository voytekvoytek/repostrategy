import java.io.*;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-17.
 */
public class FileInput implements IInputStrategy {
    Scanner reader;
    public FileInput() {
        File plik = new File("Plik.txt");
        try {
            reader= new Scanner(new FileReader(plik));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getInt() {
        return reader.nextInt();

    }

    @Override
    public String getString() {

        return reader.next();
    }

    @Override
    public double getDouble() {

        return reader.nextDouble();
    }
}
