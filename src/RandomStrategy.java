import java.util.Random;

/**
 * Created by RENT on 2017-08-17.
 */
public class RandomStrategy implements IInputStrategy {
    Random random = new Random();

    @Override
    public int getInt() {

        return random.nextInt();
    }

    @Override
    public double getDouble() {

        return random.nextDouble();
    }

    @Override
    public String getString() {
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<10; i++){
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }
}
