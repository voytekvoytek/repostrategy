import java.util.Scanner;

import static java.lang.System.in;

/**
 * Created by RENT on 2017-08-17.
 */
public class StandardInput implements IInputStrategy{


    @Override
    public int getInt() {
        System.out.println("Podaj liczbę: ");
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        return i;
    }

    @Override
    public String getString() {
        System.out.println("Podaj wyraz: ");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        return s;
    }

    @Override
    public double getDouble() {
        System.out.println("Podaj liczbę double: ");
        Scanner scanner = new Scanner(System.in);
        double d = scanner.nextDouble();
        return d;
    }
}
