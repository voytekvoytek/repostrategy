public interface IInputStrategy {
    int getInt();
    String getString();
    double getDouble();

}
