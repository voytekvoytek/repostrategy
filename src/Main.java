import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        RandomStrategy random = new RandomStrategy();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj!");
        System.out.println("Podaj typ wejścia: (1-random, 2-StandardInput, 3-plik)");

        int i = scanner.nextInt();

        if (i == 1) {
            RandomStrategy randomStrategy = new RandomStrategy();
            System.out.println("Podaj komendę: ");
            if (scanner.next().equals("getint")) {
                System.out.println(randomStrategy.getInt());
            } else if (scanner.next().equals("getstring")) {
                System.out.println(randomStrategy.getString());
            } else if (scanner.next().equals("getdouble")) {
                System.out.println(randomStrategy.getDouble());
            }
        }

        else if (i==2){
            StandardInput standardInput = new StandardInput();
            System.out.println("Podaj komendę: ");

            System.out.println(standardInput.getInt());
            System.out.println(standardInput.getDouble());
            System.out.println(standardInput.getString());
        }

    }
}

